<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Subir múltiples imágenes al servidor usando jQuery y PHP</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<h1 align="center">Subir múltiples imágenes al servidor usando jQuery y PHP </h1>
<div align="center">
<button type="button" data-toggle="modal" data-target="#src_img_upload" class="btn btn-success btn-lg">Subir archivos</button>
</div>
<br/>
<div id="image_gallery">
<?php
$images = glob("uploads/*.*");
foreach($images as $image)
{
 echo '<div class="col-md-2" align="center"><img src="' . $image .'" width="180px" height="180px" style="border:1px dotted #cacaca; margin-top:10px;"/></div>';
}
?>
</div>
</div>
<br/>
<div id="src_img_upload" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Subir multiples imágenes</h4>
</div>
<div class="modal-body">
<form method="POST" id="upload_form">
<label>Seleccion las imágenes</label>
<input type="file" name="images[]" id="img_select" multiple class='form-control'>
</form>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
 $('#img_select').change(function(){
 $('#upload_form').submit();
 });
 $('#upload_form').on('submit', function(e){
 e.preventDefault();
     $.ajax({
 url : "upload.php",
 method : "POST",
 data: new FormData(this),
 contentType:false,
 processData:false,
 success: function(data){
 $('#img_select').val('');  
                $('#src_img_upload').modal('hide');  
                $('#image_gallery').html(data); 
 }
 })
 });
});
</script>
</body>
</html>